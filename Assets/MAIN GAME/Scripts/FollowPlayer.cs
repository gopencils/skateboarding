﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FollowPlayer : MonoBehaviour
{
    public Transform target;
    bool isFollow = false;
    Rigidbody rigid;
    // Start is called before the first frame update
    void OnEnable()
    {
        rigid = GetComponent<Rigidbody>();
        //transform.DOMoveY(0.3f, 0.5f).OnComplete(() =>
        //{
        //    rigid.isKinematic = false;
        //    transform.parent = null;
        //    isFollow = true;
        //});
    }

    // Update is called once per frame
    void Update()
    {
        if(isFollow)
        {
            //if(rigid.velocity.magnitude < 75)
                rigid.AddForce((target.position - transform.position).normalized * 30);
        }
        else
        {
            if(Vector3.Distance(target.transform.position, transform.position) < 750)
            {
                OnFollow();
            }
        }
    }

    //Khi nào cần banh trồi lên lăn đến player thì gọi hàm này
    public void OnFollow()
    {
        transform.DOMoveY(0.3f, 0.5f).OnComplete(() =>
        {
            rigid.isKinematic = false;
            transform.parent = null;
            isFollow = true;
        });
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using PolyPerfect;

public class Movement : MonoBehaviour
{
    Rigidbody rigid;
    float h, v;
    Vector3 dir;
    public float speed;
    public LayerMask groundMask;
    bool isGround = true;
    public GameObject destroyParticle;
    public GameObject trail;
    RaycastHit hit;
    Coroutine botAI;
    Coroutine headDown;
    bool isHeadDown = false;
    public bool isControl = true;
    public bool isStartGame = true;
    public Animator anim;
    Transform target;
    public GameObject skateBoard;
    bool isFinish = false;
    float bonusForce;
    public GameObject powerBar;
    bool isKick = false;
    public List<Mesh> listChar = new List<Mesh>();
    public SkinnedMeshRenderer skinChar;
    public SkinnedMeshRenderer skinSkateboard;
    // Start is called before the first frame update
    void OnEnable()
    {
        isStartGame = true;
        rigid = GetComponent<Rigidbody>();
        if(CompareTag("Enemy"))
        {
            var random = Random.Range(0, listChar.Count);
            skinChar.sharedMesh = listChar[random];
            speed = Random.Range(100, 150);
            botAI = StartCoroutine(botDecision());
        }
        else
        {
            speed = 200;
            anim.Play("Start");
        }
        target = transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -68f, 68f), transform.position.y, transform.position.z);

        if (CompareTag("Player"))
        {
            trail.SetActive(true);
            if (Physics.Raycast(transform.position, Vector3.down, out hit, transform.localScale.y + 2, groundMask))
            {
                if (!isGround)
                {
                    speed = 200;
                    rigid.velocity = Vector3.zero;
                    rigid.velocity = transform.forward * speed;
                    isGround = true;
                    Debug.Log("Ground");
                    Time.timeScale = 1f;
                    Time.fixedDeltaTime = 0.02F;
                    if (isHeadDown)
                        StopCoroutine(headDown);
                    transform.GetChild(0).GetComponent<Projector>().enabled = false;
                }
            }
            else
            {
                speed = 100;
                isGround = false;
            }

            Camera.main.transform.parent.position = Vector3.Lerp(Camera.main.transform.parent.position, target.position, 15 * Time.deltaTime);
            Camera.main.transform.parent.position = new Vector3(Mathf.Clamp(Camera.main.transform.parent.position.x, -20, 26), Camera.main.transform.parent.position.y, Camera.main.transform.parent.position.z);
            if (rigid.velocity.magnitude < speed && isStartGame && !isFinish)
            {
                rigid.velocity = Vector3.Lerp(rigid.velocity, transform.forward * speed, 5 * Time.deltaTime);
            }

            if (Input.GetMouseButtonDown(0))
            {
                OnMouseDown();
            }

            if (Input.GetMouseButton(0))
            {
                OnMouseDrag();
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnMouseUp();
            }

            if (!isFinish)
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 5 * Time.deltaTime);

            if (isFinish && isStartGame && dir != Vector3.zero)
                rigid.velocity = Vector3.Lerp(rigid.velocity, new Vector3(dir.normalized.x * speed, rigid.velocity.y, rigid.velocity.z), 5 * Time.deltaTime);
        }
        else
        {
            BotMovement();
        }
    }

    void OnMouseDown()
    {
        if (CompareTag("Player") && !isGround && isControl && isStartGame && !isFinish)
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02F;
            if(isHeadDown)
                StopCoroutine(headDown);
            transform.DORotateQuaternion(Quaternion.Euler(0, 0, 0), 0f).OnComplete(() =>
            {
                rigid.AddForce(-transform.up * 5000);
            });
        }
        if (!isKick)
        {
            isKick = true;
        }
    }

    void OnMouseDrag()
    {
        if (CompareTag("Player") && isControl && isStartGame)
        {
#if UNITY_EDITOR
            h = Input.GetAxis("Mouse X");
            v = Input.GetAxis("Mouse Y");
#endif
#if UNITY_IOS
            if (Input.touchCount > 0)
            {
                h = Input.touches[0].deltaPosition.x / 10;
                v = Input.touches[0].deltaPosition.y / 10;
            }
#endif
            dir = new Vector3(h, 0, Mathf.Clamp(Mathf.Abs(v), 0.5f, 1));
            if(h > 0 && !anim.GetCurrentAnimatorStateInfo(0).IsName("IdleR") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Ending"))
            {
                anim.Play("IdleR");
            }
            else if(h < 0 && !anim.GetCurrentAnimatorStateInfo(0).IsName("IdleL") && !anim.GetCurrentAnimatorStateInfo(0).IsName("Ending"))
            {
                anim.Play("IdleL");
            }
        }
        if (!isKick)
        {
            isKick = true;
        }
    }

    void OnMouseUp()
    {
        if (CompareTag("Player") && isGround && isControl && isStartGame && !isFinish)
        {
            dir = Vector3.zero;
            Time.timeScale = 0.5f;
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
            int randomSkill = Random.Range(1, 4);
            anim.Play("Skill" + randomSkill.ToString(), 0, 0.2f);
            transform.DORotateQuaternion(Quaternion.Euler(-40, 0, 0), 0f).OnComplete(() =>
            {
                rigid.AddForce(transform.forward * 10);
                DOTween.To(() => rigid.velocity, x => rigid.velocity = x, transform.forward * speed, 0.025f).OnComplete(() =>
                {
                    transform.GetChild(0).GetComponent<Projector>().enabled = true;
                });
            });
        }
    }

    IEnumerator comeDown()
    {
        isHeadDown = true;
        yield return new WaitForSeconds(0.1f);
        if (CompareTag("Player") && !isGround)
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02F;
            if (isHeadDown)
                StopCoroutine(headDown);
            transform.DORotateQuaternion(Quaternion.Euler(0, 0, 0), 0f).OnComplete(() =>
            {
                rigid.AddForce(-transform.up * 1000);
            });
        }
        isHeadDown = false;
    }

    float botH;
    void BotMovement()
    {
        //if (Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 40, transform.localScale.y * 10, transform.localScale.z), Vector3.forward, out hit, Quaternion.Euler(0, 0, 0), 200, groundMask))
        //{
        //    if (hit.transform.CompareTag("Obstacle"))
        //    {
        //        StopCoroutine(botAI);
        //        if(hit.transform.position.x > 0)
        //        {
        //            botH = Random.Range(-1f, -0.2f);
        //        }
        //        else
        //        {
        //            botH = Random.Range(0.2f, 1f);
        //        }
        //        if (botAI != null)
        //        {
        //            botAI = StartCoroutine(botDecision());
        //        }
        //    }
        //}
        dir = new Vector3(botH, 0, Mathf.Clamp(Mathf.Abs(v), 0.5f, 1));
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 5 * Time.deltaTime);

        if (rigid.velocity.magnitude < speed)
            rigid.velocity = Vector3.Lerp(rigid.velocity, transform.forward * speed, 5 * Time.deltaTime);
        transform.GetChild(0).transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    IEnumerator botDecision()
    {
        yield return new WaitForSeconds(Random.Range(0.5f, 2f));
        if(transform.position.x > 0)
        {
            botH = Random.Range(0f, -1f);
        }
        else
        {
            botH = Random.Range(0f, 1f);
        }
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        botH = 0;
        botAI = StartCoroutine(botDecision());
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(1);
        if (CompareTag("Enemy") && gameObject != null)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Camera.main.transform.DOLocalMoveZ(-35, 0);
            transform.position = new Vector3(0, transform.position.y, transform.position.z - 300);
            dir = Vector3.zero;
            isControl = true;
            isStartGame = true;
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02F;
            anim.Play("Start");
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            anim.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    IEnumerator delayFallInHole()
    {
        yield return new WaitForSeconds(0.5f);
        anim.transform.GetChild(0).gameObject.SetActive(false);
    }

    IEnumerator Finish()
    {
        yield return new WaitForSeconds(0.5f);
        var oldForce = rigid.velocity;
        rigid.isKinematic = true;
        powerBar.SetActive(true);
        isKick = false;
        while (!isKick)
        {
            yield return null;
        }
        powerBar.SetActive(false);
        rigid.isKinematic = false;
        rigid.velocity = oldForce;
        int randomSkill = Random.Range(1, 2);
        anim.Play("Ending" + randomSkill.ToString());
        Time.timeScale = 0.5f;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        yield return new WaitForSeconds(0.8f);
        Debug.Log(powerBar.transform.GetChild(0).transform.GetChild(0).transform.localEulerAngles.z);
        rigid.AddForce(transform.forward * (12000 + (bonusForce) - (10 * Mathf.Abs(powerBar.transform.GetChild(0).transform.GetChild(0).transform.localEulerAngles.z))));
        isFinish = true;
        anim.transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
        anim.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
        skateBoard.SetActive(true);
        target = skateBoard.transform;
        skateBoard.transform.DOLocalMoveZ(0, 0.1f);
        yield return new WaitForSeconds(2);
        DOTween.To(() => rigid.velocity, x => rigid.velocity = x, Vector3.zero, 2);
        isStartGame = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Obstacle") && isStartGame)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("TeTruoc"))
            {
                isControl = false;
                isStartGame = false;
                anim.Play("TeTruoc");
            }
            StartCoroutine(Reset());
        }

        if (other.transform.CompareTag("FallBack") /*&&*/ /*isStartGame*/)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("TeSau"))
            {
                isControl = false;
                isStartGame = false;
                rigid.velocity = Vector3.zero;
                //rigid.AddForce(-transform.forward * 10000);
                anim.transform.DOKill();
                anim.transform.DOLocalMoveY(-1f, 0.2f);
                anim.Play("TeSau");
                if (CompareTag("Player"))
                {
                    Camera.main.transform.DOLocalMoveZ(Camera.main.transform.localPosition.z - 20, 0.5f);
                }
            }
            StartCoroutine(Reset());
        }

        if (other.transform.CompareTag("Hole") && isStartGame && isGround)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("TeBacThang"))
            {
                isControl = false;
                isStartGame = false;
                rigid.velocity = Vector3.zero;
                anim.Play("TeBacThang");
                StartCoroutine(delayFallInHole());
            }
            StartCoroutine(Reset());
        }

        if(other.transform.CompareTag("Player") || other.transform.CompareTag("Enemy"))
        {
            if (dir.x > 0)
            {
                float collisionForce = other.impulse.magnitude / Time.fixedDeltaTime;
                anim.Play("AttackR");
                if (collisionForce < 100)
                {
                    other.transform.GetComponent<Movement>().anim.Play("LaoDaoR");
                }
                else if (collisionForce < 200)
                {
                    other.transform.GetComponent<Movement>().anim.Play("XoNgaR");
                    Destroy(other.gameObject, 2);
                }
            }
            else if (dir.x < 0)
            {
                float collisionForce = other.impulse.magnitude / Time.fixedDeltaTime;
                anim.Play("AttackL");
                if (collisionForce < 100)
                {
                    other.transform.GetComponent<Movement>().anim.Play("LaoDaoL");
                }
                else if (collisionForce < 200)
                {
                    other.transform.GetComponent<Movement>().anim.Play("XoNgaL");
                    Destroy(other.gameObject, 2);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Slip") && isStartGame && isGround)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("TeBacThang"))
            {
                isControl = false;
                isStartGame = false;
                rigid.velocity = Vector3.zero;
                anim.Play("TeBacThang");
            }
            StartCoroutine(Reset());
        }

        if (other.CompareTag("Boost") && isStartGame)
        {
            transform.DORotateQuaternion(Quaternion.Euler(-45, 0, 0), 0.2f).OnComplete(() =>
            {
                rigid.AddForce(transform.forward * 10000);
                DOTween.To(() => rigid.velocity, x => rigid.velocity = x, transform.forward * speed, 1f);
                int randomSkill = Random.Range(1, 4);
                anim.Play("Skill" + randomSkill.ToString());
            });
        }

        if (other.CompareTag("Finish") && CompareTag("Player") && isStartGame)
        {
            transform.DORotateQuaternion(Quaternion.Euler(-45, 0, 0), 0.2f).OnComplete(() =>
            {
                rigid.AddForce(transform.forward * 10000);
                DOTween.To(() => rigid.velocity, x => rigid.velocity = x, transform.forward * speed, 1f);
                StartCoroutine(Finish());
            });
        }

        if (other.CompareTag("Coin") && isStartGame)
        {
            Destroy(other.gameObject);
        }

        if (other.CompareTag("Stunt") && isStartGame)
        {
            if (isControl)
            {
                if (transform.position.y > other.transform.position.y)
                {
                    isControl = false;
                    isStartGame = true;
                    if (CompareTag("Player"))
                    {
                        Time.timeScale = 0.5f;
                        Time.fixedDeltaTime = 0.02F * Time.timeScale;
                    }
                    other.transform.parent.GetComponent<MeshRenderer>().material.color = Color.green;
                    Debug.Log(Vector3.Distance(transform.position, other.transform.position));
                    anim.Play("LanCan2");
                    rigid.velocity = Vector3.zero;
                    rigid.velocity = transform.forward * speed;
                    anim.transform.DOLocalMoveY(-2.1f, 0.2f);
                    transform.DOMoveX(other.transform.position.x, 0.2f);
                    if (isHeadDown)
                        StopCoroutine(headDown);
                    transform.GetChild(0).GetComponent<Projector>().enabled = false;
                }
            }
        }

        if (other.CompareTag("Obstacle") && isGround && isStartGame)
        {
            Instantiate(destroyParticle, other.transform.position, Quaternion.Euler(90,0,0));
            Destroy(other.gameObject);
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("TeTruoc"))
            {
                isControl = false;
                isStartGame = false;
                anim.Play("TeTruoc");
            }
            StartCoroutine(Reset());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Stunt") && isStartGame)
        {
            isControl = true;
            isStartGame = true;
            Debug.Log("Out");
            anim.transform.DOLocalMoveY(-1, 1);
            rigid.velocity = transform.forward * speed;
            anim.SetTrigger("LanCan3");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

public class LoopMap : MonoBehaviour
{
    public static LoopMap instance;
    public float distance;
    private List<GameObject> listObject = new List<GameObject>();
    private float offsetCamera_Z;
    public static float height = 0;
    public int idPivot;
    public GameObject bot;
    public List<GameObject> listMap = new List<GameObject>();
    public List<string> listStringMap = new List<string>();
    public List<string> mapPart = new List<string>();
    int currentLevel;
    int id;

    private void Awake()
    {

    }

    private void OnEnable()
    {
        instance = this;
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        //for(int i = 0; i < listObject.Count;i++)
        //{
        //    Vector3 temp = listObject[i].transform.position;
        //    temp.z = (float)i * distance;
        //    listObject[i].transform.position = temp;

        //    //if (listObject[i].name == "Type0")
        //    //{
        //    //    var obstacleParentSpawn = Instantiate(obstacleParent);
        //    //    var spawnObstacle = Instantiate(obstacleList[Random.Range(0, obstacleList.Count)], obstacleParentSpawn.transform);
        //    //    //spawnObstacle.SetActive(false);
        //    //    //spawnObstacle.transform.parent = obstacleParentSpawn.transform;
        //    //    obstacleParentSpawn.transform.parent = listObject[i].transform;
        //    //    obstacleParentSpawn.transform.position = listObject[i].transform.position;
        //    //    obstacleParentSpawn.transform.DOMoveX(0, 0).OnComplete(() =>
        //    //    {
        //    //        //spawnObstacle.transform.parent = null;
        //    //        //spawnObstacle.SetActive(true);
        //    //        StartCoroutine(delaySpawn(spawnObstacle));
        //    //    }); ;

        //    //    for (int j = 0; j < 2; j++)
        //    //    {
        //    //        var spawnBot = Instantiate(listBot[Random.Range(0, listBot.Count)]);
        //    //        spawnBot.transform.position = listObject[i].transform.position;
        //    //        spawnBot.transform.DOMoveX(Random.Range(-40, 40), 0);
        //    //        allBot.Add(spawnBot);
        //    //        //spawnBot.transform.DOMoveZ(i, 0);
        //    //    }
        //    //}
        //}
        mapPart = listStringMap[currentLevel].Split(' ').ToList();
        for(int i = 0; i < mapPart.Count; i++)
        {
            if (id == 35 || id == 36)
            {
                height -= 7.5f;
            }
            else
            {
                id = int.Parse(mapPart[i]);
                var spawnPart = Instantiate(listMap[id]);
                spawnPart.transform.parent = transform;
                height += spawnPart.transform.localPosition.y;
                spawnPart.transform.localPosition = new Vector3(-15, height, i * 100);
                spawnPart.SetActive(true);
            }
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            listObject.Add(transform.GetChild(i).gameObject);
        }
        offsetCamera_Z = Camera.main.transform.position.z;
        idPivot = 7;
        for (int i = idPivot + 1; i < listObject.Count; i++)
        {
            listObject[i].SetActive(false);
        }
    }

    private void Update()
    {
        float z1 = listObject[0].transform.position.z;
        float z2 = Camera.main.transform.position.z;

        if (z2 - z1 >= (distance + offsetCamera_Z))
        {
            GameObject obj = listObject[0];
            obj.SetActive(false);
            listObject.RemoveAt(0);
            listObject[idPivot].SetActive(true);
        }
        //float z1 = listObject[0].transform.position.z;
        //float z2 = Camera.main.transform.position.z;

        //if(z2 - z1 >= (distance + offsetCamera_Z))
        //{
        //    GameObject obj = listObject[0];
        //    obj.SetActive(false);
        //    listObject.RemoveAt(0);
        //    obj.transform.position = listObject[listObject.Count - 1].transform.position + Vector3.forward * distance;
        //    //if (obj.name == "Type0")
        //    //{
        //    //    height -= 4;

        //    //    var obstacleParentSpawn = Instantiate(obstacleParent);
        //    //    var spawnObstacle = Instantiate(obstacleList[Random.Range(0, obstacleList.Count)], obstacleParentSpawn.transform);
        //    //    //spawnObstacle.SetActive(false);
        //    //    //spawnObstacle.transform.parent = obstacleParentSpawn.transform;
        //    //    obstacleParentSpawn.transform.parent = obj.transform;
        //    //    obstacleParentSpawn.transform.position = obj.transform.position;
        //    //    obstacleParentSpawn.transform.DOMoveX(0, 0).OnComplete(() =>
        //    //    {
        //    //        //spawnObstacle.transform.parent = null;
        //    //        //spawnObstacle.SetActive(true);
        //    //        StartCoroutine(delaySpawn(spawnObstacle));
        //    //    }); ;

        //    //    for (int i = 0; i < 2; i++)
        //    //    {
        //    //        allBot.RemoveAll(item => item == null);
        //    //        if (allBot.Count < 15)
        //    //        {
        //    //            var spawnBot = Instantiate(listBot[Random.Range(0, listBot.Count)]);
        //    //            spawnBot.transform.position = obj.transform.position;
        //    //            spawnBot.transform.DOMoveX(Random.Range(-40, 40), 0);
        //    //            allBot.Add(spawnBot);
        //    //            //spawnBot.transform.DOMoveZ(i, 0);
        //    //        }
        //    //    }
        //    //}
        //    obj.transform.DOLocalMoveY(height, 0);
        //    obj.SetActive(true);
        //    listObject.Add(obj);
        //}
    }
}

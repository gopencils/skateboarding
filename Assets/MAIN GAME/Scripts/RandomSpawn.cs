﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    GameObject target;
    public float offSet;
    public float RangeY;
    public float randomSpaceY;
    public float defaultMaxX = 3f;

    // Start is called before the first frame update
    void OnEnable()
    {
        target = transform.GetChild(0).gameObject;
        for(int i = 1; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        for (float y = -RangeY; y < RangeY; y += RangeY)
        {
            float maxX = defaultMaxX;
            float minX = 0f;
            if (y == 0)
            {
                y = Random.Range(-randomSpaceY, randomSpaceY);
            }
            else
            {
                maxX = defaultMaxX - 0.5f;
                minX = 0.5f;
            }
            for (float x = minX; x <= maxX; x += 1)
            {
                var spawn = Instantiate(target, transform);
                spawn.SetActive(false);
                spawn.transform.localPosition = new Vector3(target.transform.localPosition.x + x * offSet + (Random.Range(-0.25f, 0.25f)), target.transform.localPosition.y, target.transform.localPosition.z + y * offSet + (Random.Range(-randomSpaceY, randomSpaceY)));
                spawn.SetActive(true);
            }
            target.SetActive(false);
        }
    }

    private void OnDisable()
    {
        target.SetActive(true);
    }
}
